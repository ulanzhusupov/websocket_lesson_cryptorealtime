import 'dart:convert';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:websocket_realtime_currency/data/models/currency_model.dart';
import 'package:websocket_realtime_currency/presentation/pages/currency_graphic.dart';

class CurrencyPage extends StatefulWidget {
  const CurrencyPage({super.key});

  @override
  State<CurrencyPage> createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  final channel = WebSocketChannel.connect(
      Uri.parse('wss://stream.binance.com:9443/ws/etheur@trade'));

  List<FlSpot> priceData = [];
  // List<PriceModel> priceData = [];
  List<Color> gradientColors = [
    Colors.cyan,
    Colors.blue,
  ];

  bool showAvg = false;

  void updateChart(double price) {
    priceData.add(FlSpot(priceData.length.toDouble(), price));
    // setState(() {
    //   // Добавление новой точки с текущей ценой
    // });
  }

  String handleTime(String jsonString) {
    try {
      // Десериализация JSON
      Map<String, dynamic> jsonData = json.decode(jsonString);

      // Получение значения поля "p" (цена)
      String time = jsonData['E'];

      // Вывод значения цены
      return time;
    } catch (e) {
      print('Ошибка при обработке JSON: $e');
      return "";
    }
  }

  String handlePrice(String jsonString) {
    try {
      // Десериализация JSON
      Map<String, dynamic> jsonData = json.decode(jsonString);

      // Получение значения поля "p" (цена)
      String price = jsonData['p'];

      // Вывод значения цены
      return price;
    } catch (e) {
      print('Ошибка при обработке JSON: $e');
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<dynamic>(
          stream: channel.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              if (snapshot.hasData) {
                // Допустим, ваша модель приходит в формате Map<String, dynamic>
                // Map<String, dynamic> jsonData = snapshot.data;

                // Создаем экземпляр PriceModel из данных
                PriceModel newPrice = PriceModel(
                  time: DateTime.parse(handleTime(
                      snapshot.data)), // или получите время из jsonData
                  price: double.parse(handlePrice(
                      snapshot.data)), // или другой способ получения цены
                );

                // Обновляем данные и добавляем новую цену
                // updateChart(
                //     double.parse(handleWebSocketResponse(snapshot.data)));
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(handlePrice(snapshot.data)),
                      const LineChartSample2(),
                      LineChart(
                        LineChartData(
                          lineBarsData: [
                            LineChartBarData(
                              spots: [
                                FlSpot(double.parse(handleTime(snapshot.data)),
                                    double.parse(handlePrice(snapshot.data))),
                              ],
                            ),
                          ],
                          titlesData: FlTitlesData(
                            bottomTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                getTitlesWidget: (value, title) =>
                                    Text('${value.toInt()}'),
                              ),
                            ),
                            leftTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                getTitlesWidget: (value, title) =>
                                    Text('${value.toInt()}'),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
            }
            return const SizedBox();
          }),
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        horizontalInterval: 1,
        verticalInterval: 1,
        getDrawingHorizontalLine: (value) {
          return const FlLine(
            color: Colors.red,
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: Colors.amber,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 30,
            interval: 1,
            getTitlesWidget: bottomTitleWidgets,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            interval: 1,
            getTitlesWidget: leftTitleWidgets,
            reservedSize: 42,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border.all(color: const Color(0xff37434d)),
      ),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 3),
            FlSpot(2.6, 2),
            FlSpot(4.9, 5),
            FlSpot(6.8, 3.1),
            FlSpot(8, 4),
            FlSpot(9.5, 3),
            FlSpot(11, 4),
          ],
          isCurved: true,
          gradient: LinearGradient(
            colors: gradientColors,
          ),
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradient: LinearGradient(
              colors: gradientColors
                  .map((color) => color.withOpacity(0.3))
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text;
    switch (value.toInt()) {
      case 2:
        text = const Text('MAR', style: style);
        break;
      case 5:
        text = const Text('JUN', style: style);
        break;
      case 8:
        text = const Text('SEP', style: style);
        break;
      default:
        text = const Text('', style: style);
        break;
    }

    return SideTitleWidget(
      axisSide: meta.axisSide,
      child: text,
    );
  }

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );
    String text;
    switch (value.toInt()) {
      case 1:
        text = '10K';
        break;
      case 3:
        text = '30k';
        break;
      case 5:
        text = '50k';
        break;
      default:
        return Container();
    }

    return Text(text, style: style, textAlign: TextAlign.left);
  }
}

class PriceModel {
  final DateTime time;
  final double price;

  PriceModel({required this.time, required this.price});
}
