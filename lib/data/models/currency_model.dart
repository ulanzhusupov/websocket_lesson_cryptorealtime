class CurrencyModel {
  String? e;
  int? e2;
  String? s;
  int? t;
  String? price;
  String? q;
  int? b;
  int? a;
  int? t2;
  bool? m;
  bool? m2;

  CurrencyModel(
      {this.e,
      this.e2,
      this.s,
      this.t,
      this.price,
      this.q,
      this.b,
      this.a,
      this.t2,
      this.m,
      this.m2});

  CurrencyModel.fromJson(Map<String, dynamic> json) {
    e = json['e'];
    e2 = json['E'];
    s = json['s'];
    t = json['t'];
    price = json['p'];
    q = json['q'];
    b = json['b'];
    a = json['a'];
    t2 = json['T'];
    m = json['m'];
    m2 = json['M'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['e'] = e;
    data['E'] = e2;
    data['s'] = s;
    data['t'] = t;
    data['p'] = price;
    data['q'] = q;
    data['b'] = b;
    data['a'] = a;
    data['T'] = t2;
    data['m'] = m;
    data['M'] = m2;
    return data;
  }
}
